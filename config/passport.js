var LocalStrategy = require('passport-local').Strategy,
	FacebookStrategy = require('passport-facebook').Strategy,
	TwitterStrategy = require('passport-twitter').Strategy,
	GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
	SoundcloudStrategy = require('passport-soundcloud').Strategy,
	User = require('../app/models/user'),
	configAuth = require('./auth');

module.exports = function(passport) {

	passport.serializeUser(function(user, done) {
		// store only id to keep session efficient
		done(null, user._id);
	});

	passport.deserializeUser(function(id, done) {
		// return user from db by stored id
		User.findById(id, function(err, user) {
			done(err, user);
		});
	});

	// local signup
	passport.use('local-signup', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	function(req, email, password, done) {
		process.nextTick(function() {
			// search db for matching email
			User.findOne({'local.email': email}, function(err, user) {
				if(err) {
					// database error
					return done({message: err.msg}, null);
				}
				if(user) {
					// user found, someone already registered with this email address, return error
					return done([{field: 'local.email', message: req.i18n.t('ns.auth:errors.alreadyRegistered')}], null);
				} else {
					// all good, user doesn't exist, create new user from schema
					var newUser = new User();
					newUser.local.email = email;
					newUser.local.password = newUser.generateHash(password);
					// remove email and password from object, we already have them and don't want to duplicate
					delete req.body.email;
					delete req.body.password;
					// copy remaining fields over to user object
					newUser.local = req.body;
					// save user to db
					newUser.save(function(err) {
						if(err) {
							// something went wrong, return error
							return done(err, null);
						}
						// all good, return new user object
						return done(null, newUser);
					});
				}
			});
		});
	}));

	// local login
	passport.use('local-login', new LocalStrategy({
		usernameField: 'email',
		passwordField: 'password',
		passReqToCallback: true
	},
	function(req, email, password, done) {
		process.nextTick(function() {
			// search for user with matching email
			User.findOne({'local.email': email}, function(err, user) {
				if(err) {
					// database error, pass it back
					return done({message: err.msg}, null);
				}
				if(!user) {
					// user not found, return error
					return done([{field: 'local.email', message: req.i18n.t('ns.auth:errors.userNotFound')}], null);
				}
				if(!user.validPassword(password)) {
					// wrong password, return error
					return done([{field: 'local.password', message: req.i18n.t('ns.auth:errors.invalidPassword')}], null);
				}
				// all good, return user
				return done(null, user);
			});
		});
	}));

	passport.use(new FacebookStrategy({
		clientID			: configAuth.facebookAuth.clientID,
		clientSecret		: configAuth.facebookAuth.clientSecret,
		callbackURL			: configAuth.facebookAuth.callbackURL,
		passReqToCallback	: true
	},
	function(req, token, refreshToken, profile, done) {
		process.nextTick(function() {
			// check if user already logged in
			if(!req.user) {
				// we're not, find user with matching facebook id
				User.findOne({'facebook.id': profile.id}, function(err, user) {
					if(err) {
						// database error, pass it back
						return done(err);
					}
					if(user) {
						// user found, pass it back
						return done(null, user);
					} else {
						// user not found, copy fields from facebook profile to user object
						var newUser = new User();
						newUser.facebook.id = profile.id;
						newUser.facebook.token = token;
						newUser.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
						newUser.facebook.email = profile.emails[0].value;
						// and save user
						newUser.save(function(err) {
							if(err) {
								// database error, pass it back
								throw err;
							}
							// success, return user
							return done(null, newUser);
						});
					}
				});
			} else {
				// user is logged in, copy facebook profile fields to session user
				req.user.facebook.id = profile.id;
				req.user.facebook.token = token;
				req.user.facebook.name = profile.name.givenName + ' ' + profile.name.familyName;
				req.user.facebook.email = profile.emails[0].value;
				// and save it
				req.user.save(function(err) {
					if(err) {
						// database error pass it back
						throw err;
					}
					// success, return user object
					return done(null, req.user);
				});
			}
		});
	}));

	passport.use(new TwitterStrategy({
		consumerKey			: configAuth.twitterAuth.consumerKey,
		consumerSecret		: configAuth.twitterAuth.consumerSecret,
		callbackURL			: configAuth.twitterAuth.callbackURL,
		passReqToCallback	: true
	},
	function(req, token, tokenSecret, profile, done) {
		process.nextTick(function() {
			// check if user already logged in
			if(!req.user) {
				// user not logged in, are they in database?
				User.findOne({'twitter.id': profile.id}, function(err, user) {
					if(err) {
						// database error pass it back
						return done(err);
					}
					if(user) {
						// user exists in db, pass back database user
						return done(null, user);
					} else {
						// not found, create new user
						var newUser = new User();
						// copy twitter profile feeds to user object
						newUser.twitter.id = profile.id;
						newUser.twitter.token = token;
						newUser.twitter.username = profile.username;
						newUser.twitter.displayName = profile.displayName;
						// and save the user
						newUser.save(function(err) {
							if(err) {
								// database error pass it back
								throw err;
							}
							// return new user object
							return done(null, newUser);
						});
					}
				});
			} else {
				// user logged in, copy twitter profile to user object
				req.user.twitter.id = profile.id;
				req.user.twitter.token = token;
				req.user.twitter.username = profile.username;
				req.user.twitter.displayName = profile.displayName;
				// save user
				req.user.save(function(err) {
					if(err) {
						// database error pass it back
						throw err;
					}
					// return updated user object
					return done(null, req.user);
				});
			}
		});
	}));

	passport.use(new GoogleStrategy({
		clientID			: configAuth.googleAuth.clientID,
		clientSecret		: configAuth.googleAuth.clientSecret,
		callbackURL			: configAuth.googleAuth.callbackURL,
		passReqToCallback	: true
	},
	function(req, token, refreshToken, profile, done) {
		process.nextTick(function() {
			// is user logged in?
			if(!req.user) {
				// user not logged in, search db for user
				User.findOne({'google.id': profile.id}, function(err, user) {
					if(err) {
						// database error pass it back
						return done(err);
					}
					if(user) {
						// user found, pass it back
						return done(null, user);
					} else {
						// user not found, create new user
						var newUser = new User();
						// copy google profile items to user object
						newUser.google.id = profile.id;
						newUser.google.token = token;
						newUser.google.name = profile.displayName;
						newUser.google.email = profile.emails[0].value;
						// save user object
						newUser.save(function(err) {
							if(err) {
								// database error pass it back
								throw err;
							}
							// all good, return new user
							return done(null, newUser);
						});
					}
				});
			} else {
				// user logged in, copy google profile to user object
				req.user.google.id = profile.id;
				req.user.google.token = token;
				req.user.google.name = profile.displayName;
				req.user.google.email = profile.emails[0].value;
				// save user
				req.user.save(function(err) {
					if(err) {
						// database error pass it back
						throw err;
					}
					// success, return updated user
					return done(null, req.user);
				});
			}
		});
	}));

	passport.use(new SoundcloudStrategy({
		clientID			: configAuth.soundcloudAuth.clientID,
		clientSecret		: configAuth.soundcloudAuth.clientSecret,
		callbackURL			: configAuth.soundcloudAuth.callbackURL,
		passReqToCallback	: true
	},
	function(req, token, refreshToken, profile, done) {
		process.nextTick(function() {
			// is user logged in?
			if(!req.user) {
				// not logged in, find user in db
				User.findOne({'soundcloud.id': profile.id}, function(err, user) {
					if(err) {
						// database error pass it back
						return done(err);
					}
					if(user) {
						// user found, pass it back
						return done(null, user);
					} else {
						// no such user, create new user
						var newUser = new User();
						// populate user from soundcloud profile
						newUser.soundcloud.id = profile.id;
						newUser.soundcloud.token = token;
						newUser.soundcloud.displayName = profile.displayName;
						// save new user
						newUser.save(function(err) {
							if(err) {
								// database error pass it back
								throw err;
							}
							// pass back new user
							return done(null, newUser);
						});
					}
				});
			} else {
				// user is logged in, populate profile from soundcloud
				req.user.soundcloud.id = profile.id;
				req.user.soundcloud.token = token;
				req.user.soundcloud.displayName = profile.displayName;
				// save updated user
				req.user.save(function(err) {
					if(err) {
						// database error pass it back
						throw err;
					}
					// return updated user
					return done(null, req.user);
				});
			}
		});
	}));
};

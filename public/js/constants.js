/* global app:true */

(function() {
	'use strict';
}());

// set up account types
app.constant('ACCOUNT_TYPES', {
	accountTypes: [
		{
			id: 'admin',
			name: 'Admin',
			selectable: false
		},
		{
			id: 'buyer',
			name: 'Buyer',
			selectable: true
		},
		{
			id: 'seller',
			name: 'Seller',
			selectable: true
		}
	]
});

// event constants for auth
app.constant('AUTH_EVENTS', {
	loginSuccess: 'auth-login-success',
	loginFailed: 'auth-login-failed',
	signupSuccess: 'auth-signup-success',
	signupFailed: 'auth-signup-failed',
	logoutSuccess: 'auth-logout-success',
	logoutFailed: 'auth-logout-failed',
	sessionTimeout: 'auth-session-timeout',
	notAuthenticated: 'auth-not-authenticated',
	notAuthorized: 'auth-not-authorized'
});

// list of countries and their associated states/provinces
app.constant('COUNTRIES', {
	countries: [
		{
			id: 'CAN',
			name: 'Canada',
			states: [
				{
					id: 'AB',
					name: 'Alberta'
				},
				{
					id: 'BC',
					name: 'British Columbia'
				},
				{
					id: 'MB',
					name: 'Manitoba'
				},
				{
					id: 'NB',
					name: 'New Brunswick'
				},
				{
					id: 'NL',
					name: 'Newfoundland and Labrador'
				},
				{
					id: 'NS',
					name: 'Nova Scotia'
				},
				{
					id: 'ON',
					name: 'Ontario'
				},
				{
					id: 'PE',
					name: 'Prince Edward Island'
				},
				{
					id: 'QC',
					name: 'Quebec'
				},
				{
					id: 'SK',
					name: 'Saskatchewan'
				},
				{
					id: 'NT',
					name: 'Northwest Territories'
				},
				{
					id: 'NU',
					name: 'Nunavut'
				},
				{
					id: 'YT',
					name: 'Yukon'
				}
			]
		},
		{
			id: 'USA',
			name: 'United States',
			states: [
				{
					id: 'AL',
					name: 'Alabama'
				},
				{
					id: 'AK',
					name: 'Alaska'
				},
				{
					id: 'AZ',
					name: 'Arizona'
				},
				{
					id: 'AR',
					name: 'Arkansas'
				},
				{
					id: 'CA',
					name: 'California'
				},
				{
					id: 'CO',
					name: 'Colorado'
				},
				{
					id: 'CT',
					name: 'Connecticut'
				},
				{
					id: 'DE',
					name: 'Delaware'
				},
				{
					id: 'DC',
					name: 'District Of Columbia'
				},
				{
					id: 'FL',
					name: 'Florida'
				},
				{
					id: 'GA',
					name: 'Georgia'
				},
				{
					id: 'HI',
					name: 'Hawaii'
				},
				{
					id: 'ID',
					name: 'Idaho'
				},
				{
					id: 'IL',
					name: 'Illinois'
				},
				{
					id: 'IN',
					name: 'Indiana'
				},
				{
					id: 'IA',
					name: 'Iowa'
				},
				{
					id: 'KS',
					name: 'Kansas'
				},
				{
					id: 'KY',
					name: 'Kentucky'
				},
				{
					id: 'LA',
					name: 'Louisiana'
				},
				{
					id: 'ME',
					name: 'Maine'
				},
				{
					id: 'MD',
					name: 'Maryland'
				},
				{
					id: 'MA',
					name: 'Massachusetts'
				},
				{
					id: 'MI',
					name: 'Michigan'
				},
				{
					id: 'MN',
					name: 'Minnesota'
				},
				{
					id: 'MS',
					name: 'Mississippi'
				},
				{
					id: 'MO',
					name: 'Missouri'
				},
				{
					id: 'MT',
					name: 'Montana'
				},
				{
					id: 'NE',
					name: 'Nebraska'
				},
				{
					id: 'NV',
					name: 'Nevada'
				},
				{
					id: 'NH',
					name: 'New Hampshire'
				},
				{
					id: 'NJ',
					name: 'New Jersey'
				},
				{
					id: 'NM',
					name: 'New Mexico'
				},
				{
					id: 'NY',
					name: 'New York'
				},
				{
					id: 'NC',
					name: 'North Carolina'
				},
				{
					id: 'ND',
					name: 'North Dakota'
				},
				{
					id: 'OH',
					name: 'Ohio'
				},
				{
					id: 'OK',
					name: 'Oklahoma'
				},
				{
					id: 'OR',
					name: 'Oregon'
				},
				{
					id: 'PA',
					name: 'Pennsylvania'
				},
				{
					id: 'RI',
					name: 'Rhode Island'
				},
				{
					id: 'SC',
					name: 'South Carolina'
				},
				{
					id: 'SD',
					name: 'South Dakota'
				},
				{
					id: 'TN',
					name: 'Tennessee'
				},
				{
					id: 'TX',
					name: 'Texas'
				},
				{
					id: 'UT',
					name: 'Utah'
				},
				{
					id: 'VT',
					name: 'Vermont'
				},
				{
					id: 'VA',
					name: 'Virginia'
				},
				{
					id: 'WA',
					name: 'Washington'
				},
				{
					id: 'WV',
					name: 'West Virginia'
				},
				{
					id: 'WI',
					name: 'Wisconsin'
				},
				{
					id: 'WY',
					name: 'Wyoming'
				}
			]
		}
	]
});

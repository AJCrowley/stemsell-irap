/* global app:true */

(function() {
	'use strict';
}());

var app = angular.module('co.stemsell.Alpha', ['ngResource', 'ui.router', 'flow']);

// set up routes
app.config(function($locationProvider, $stateProvider, $urlRouterProvider) {
	// turn off reload on location update
	$locationProvider.html5Mode(true);
	// set up states
	$stateProvider
		.state('home', {
			url: '/',
			templateUrl: '/views/partials/home'
		})
		.state('login', {
			url: '/login',
			templateUrl: '/views/partials/login'
		})
		.state('signup', {
			url: '/signup',
			templateUrl: '/views/partials/signup'
		})
		.state('mytracks', {
			url: '/mytracks',
			templateUrl: '/views/partials/mytracks'
		})
		.state('profile', {
			url: '/profile',
			templateUrl: '/views/partials/profile'
		});
});

// set flow defaults
app.config(['flowFactoryProvider', function(flowFactoryProvider) {
    flowFactoryProvider.defaults = {
		target: '/upload',
		permanentErrors: [404, 500, 501],
		maxChunkRetries: 1,
		chunkRetryInterval: 5000,
		simultaneousUploads: 4
    };
}]);

/* global app:true */

(function() {
	'use strict';
}());

app.factory('TranslateService', function($location, $rootScope, $resource) {
	// declare route
	var translateResource = $resource('/translate', null, {
			translate: {
				method: 'POST'
			}
		});

	translateService = {
		// translate function
		translate: function(key, callback, error) {
			// default callback and error to noop if not provided
			var callback = callback || angular.noop,
				error = error || angular.noop;
			// call resource
			translateResource.translate({
				key: key
			}, function(value) {
				return callback(value);
			}, function(err) {
				return error(err.data);
			});
		}
	};
	// return handle
	return translateService;
});

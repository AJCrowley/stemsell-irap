/* global app:true */

(function() {
	'use strict';
}());

app.factory('AuthService', function($location, $rootScope, $resource) {
	// declare routes on server resource
	var authResource = $resource('/auth', null, {
			login: {
				method: 'POST',
				url: '/auth/login'
			},

			signup: {
				method: 'POST',
				url: '/auth/signup'
			},

			saveProfile: {
				method: 'POST',
				url: '/auth/save'
			},

			logout: {
				method: 'POST',
				url: '/auth/logout'
			},

			getLoggedInUser: {
				method: 'POST',
				url: '/auth/getuser'
			}
		});

	authService = {
		// login function
		login: function(email, password, callback, error) {
			// if callback or error not passed, default to noop
			var callback = callback || angular.noop,
				error = error || angular.noop;
			authResource.login({
				email: email,
				password: password
			}, function(user) {
				return callback(user);
			}, function(err) {
				return error(err.data);
			});
		},

		// signup function
		signup: function(user, callback, error) {
			// if callback or error not passed, default to noop
			var callback = callback || angular.noop,
				error = error || angular.noop;
			authResource.signup(user, function(user) {
				return callback(user);
			}, function(err) {
				return error(err.data);
			});
		},

		// save profile function
		saveProfile: function(user, callback, error) {
			// if callback or error not passed, default to noop
			var callback = callback || angular.noop,
				error = error || angular.noop;
			authResource.saveProfile(user, function(user) {
				return callback(user);
			}, function(err) {
				return error(err.data);
			});
		},

		// logout function
		logout: function(callback, error) {
			// if callback or error not passed, default to noop
			var callback = callback || angular.noop,
				error = error || angular.noop;
			authResource.logout(function() {
				return callback();
			}, function(err) {
				return error(err.data);
			});
		},

		// request logged in user from server
		getLoggedInUser: function(callback, error) {
			// if callback or error not passed, default to noop
			var callback = callback || angular.noop,
				error = error || angular.noop;
			authResource.getLoggedInUser(function(user) {
				return callback(user);
			}, function(err) {
				return error(err.data);
			});
		}
	};

	// pass back service handle
	return authService;
});

/* global app:true */

(function() {
	'use strict';
}());

app.controller('ProfileController', function($scope, $rootScope, AuthService, AUTH_EVENTS, COUNTRIES, ACCOUNT_TYPES) {
	// copy constants to local
	$scope.countries = COUNTRIES.countries;
	$scope.accountTypes = ACCOUNT_TYPES.accountTypes;

	$scope.saveProfile = function(form) {
		// does form pass internal validation?
		if(form.$valid) {
			// pass to auth service
			AuthService.saveProfile($scope.loggedInUser, function(user) {
				// profile saved
				console.log('done: ' + JSON.stringify(user));
			}, function(err) {
				// unable to save profile
				console.log('err: ' + JSON.stringify(err));
			});
		}
	}
});

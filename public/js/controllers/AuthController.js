/* global app:true */

(function() {
	'use strict';
}());

app.controller('AuthController', function($scope, $rootScope, $filter, AuthService, AUTH_EVENTS) {
	// establish empty default credentials set
	$scope.credentials = {
		email: '',
		password: '',
		country: null
	};

	$scope.login = function(form) {
		// pass email and pass to login service
		AuthService.login($scope.credentials.email, $scope.credentials.password, function(promise) {
			// success, broadcast event
			$rootScope.$broadcast(AUTH_EVENTS.loginSuccess, promise.user);
		}, function(err) {
			// failure, broadcast event
			$rootScope.$broadcast(AUTH_EVENTS.loginFailed, err);
			// parse error object and apply errors
			/*angular.forEach(err, function(value, key) {
				console.log(value);
			});*/
		});
	};

	$scope.signup = function(form) {
		// check form is valid
		if(form.$valid) {
			// pass credentials to signup in auth service
			AuthService.signup($scope.credentials, function(user) {
				// success, broadcast event
				$rootScope.$broadcast(AUTH_EVENTS.signupSuccess);
			}, function(err) {
				// failure, broadcast error
				$rootScope.$broadcast(AUTH_EVENTS.signupFailed, err);
			});
		}
	}

	$scope.logout = function() {
		// call logout in auth service
		AuthService.logout(function() {
			// success, broadcast event
			$rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
		}, function(err) {
			// error, let everyone know
			$rootScope.$broadcast(AUTH_EVENTS.logoutFailed, err);
		});
	}
});

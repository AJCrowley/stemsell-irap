/* global app:true */

(function() {
	'use strict';
}());

app.controller('StemsellController', function($scope, $state, AuthService, AUTH_EVENTS, COUNTRIES, ACCOUNT_TYPES) {
	// import constants to local
	$scope.countries = COUNTRIES.countries;
	$scope.accountTypes = ACCOUNT_TYPES.accountTypes;

	// call auth service to see if we're already logged in
	AuthService.getLoggedInUser(function(result) {
		// copy result to logged in user, result will be null if no user logged in
		$scope.loggedInUser = result.user;
	}, function(err) {
		// server error
		console.log(err);
	});

	// listen for successful login event
	$scope.$on(AUTH_EVENTS.loginSuccess, function(e, user) {
		// copy logged in user to local
		$scope.loggedInUser = user;
		// redirect to home page
		$state.transitionTo('home');
	});

	// listen for logout event
	$scope.$on(AUTH_EVENTS.logoutSuccess, function(e) {
		// clear logged in user
		$scope.loggedInUser = null;
	});

	// listen for sign up event
	$scope.$on(AUTH_EVENTS.signupSuccess, function(e, user) {
		// copy signed up user
		$scope.loggedInUser = user;
		// redirect to home
		$state.transitionTo('home');
	})
});

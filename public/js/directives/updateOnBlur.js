/* global app:true */

(function() {
    'use strict';
}());

app.directive('updateOnBlur', function() {
    return {
        restrict: 'A',
        priority: 100,
        require: 'ngModel',
        link: function(scope, elm, attr, ngModelCtrl) {
            // ignore radio and checkboxes
            if (attr.type === 'radio' || attr.type === 'checkbox') return;
            // remove bound events for keydown and change
            elm.unbind('input').unbind('keydown').unbind('change');
            // and bind onblur event
            elm.bind('blur', function() {
                scope.$apply(function() {
                    // update model
                    ngModelCtrl.$setViewValue(elm.val());
                });
            });
        }
    };
});

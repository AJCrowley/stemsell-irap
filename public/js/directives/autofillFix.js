/* global app:true */

(function() {
	'use strict';
}());

// autofill in browsers doesn't always trigger change or keydown events on inputs
// so let's bind to submit event and manually trigger all these events
app.directive('autofillFix', function($timeout) {
	return function(scope, element, attrs) {
		element.prop('method', 'post');
		if (attrs.ngSubmit) {
			$timeout(function() {
				element
					.unbind('submit')
					.bind('submit', function(event) {
						event.preventDefault();
						element
							.find('input, textarea, select')
							.trigger('input')
							.trigger('change')
							.trigger('keydown');
							scope.$apply(attrs.ngSubmit);
					});
			});
		}
	};
});

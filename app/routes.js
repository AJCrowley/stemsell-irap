var multipart = require('connect-multiparty'),
	flow = require('./flow-node.js')('tmp'),
	multipartMiddleware = multipart();

module.exports = function(app, passport) {
	// forward any partial request to partials dir
	app.get('/views/partials/:partial', function(req, res) {
		res.render('partials/' + req.params.partial + '.ejs');
	});

	app.post('/auth/login', function(req, res) {
		// auth with local-login strategy
		passport.authenticate('local-login', function(err, user) {
			if(err) {
				// unauthorized http status with specific error
				return res.status(401).json(err);
			} else {
				// all good, start the login session
				req.logIn(user, function(err) {
					if(err) {
						// something went wrong
						return next(err);
					}
					// all good, pass user back with http ok
					return res.status(200).json({user: user});
				});
			}
		})(req, res);
	});

	app.post('/auth/signup', function(req, res) {
		// auth with local signup strategy
		passport.authenticate('local-signup', function(err, user) {
			if(err) {
				// something went wrong, pass back err object
				return res.status(400).json(err);
			} else {
				// all good, pass back user
				return res.status(200).json({user: user});
			}
		})(req, res);
	});

	app.post('/auth/save', function(req, res) {
		// is user authenticated?
		if(req.isAuthenticated()) {
			// copy local details to user account
			req.user.local = req.body.local;
			// and save
			req.user.save(function(err) {
				if(err) {
					// something went wrong, return error
					return res.status(400).json(err);
				}
				// all good, return user
				return res.status(200).json({user: req.user});
			});
		}
	});

	app.post('/auth/getuser', function(req, res) {
		// return user object (will be null if not logged in)
		return res.status(200).json({user: req.user});
	});

	app.post('/auth/logout', function(req, res) {
		// destroy all session data
		req.logout();
		// return empty "ok"
		return res.status(200).json();
	});

	app.post('/upload', multipartMiddleware, function(req, res) {
		flow.post(req, function(status, filename, identifier) {
	        console.log('POST', status, identifier);
			if(status == 'done') {
				console.log('all done', filename);
			}
	        res.status(200).end();
	    });
	});

	// Handle status checks on chunks through Flow.js
	app.get('/upload', function(req, res) {
	    flow.get(req, function(status, filename, original_filename, identifier) {
	        console.log('GET', status);
	        res.status(status == 'found' ? 200 : 404).end();
	    });
	});

	app.get('/download/:identifier', function(req, res) {
	    flow.write(req.params.identifier, res);
	});

	// forward root to index
	app.get(['/', '/:route'], function(req, res) {
		res.render('index.ejs');
	});
};

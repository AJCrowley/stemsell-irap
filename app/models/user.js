// User model!

var mongoose = require('mongoose'),
	bcrypt = require('bcrypt-nodejs'),
	userSchema = mongoose.Schema({
		local			: {
			email		: {
							type: String,
							required: true,
							validate: [
								function(v) {
									// regex to validate real email address
									return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v);
								},
								'invalidEmail'
							]
						},
			firstName	: {
							type: String,
							required: true
						},
			lastName	: {
							type: String,
							required: true
						},
			password	: {
							type: String,
							required: true
						},
			address1	: {
							type: String,
							required: true
						},
			address2	: String,
			city		: {
							type: String,
							required: true
						},
			state		: {
							type: String,
							required: true
						},
			zip			: {
							type: String,
							required: true
						},
			country			: {
							type: String,
							required: true
						},
			paypal		: String,
			accountType	: {
							type: String,
							required: true
						},
			active		: {
							type: Boolean,
							required: true,
							default: false
						},
		},
		facebook		: {
			id			: String,
			token		: String,
			email		: String,
			name		: String,
		},
		twitter			: {
			id			: String,
			token		: String,
			displayName	: String,
			username	: String,
		},
		google			: {
			id			: String,
			token		: String,
			email		: String,
			name		: String,
		},
		soundcloud		: {
			id			: String,
			token		: String,
			displayName	: String,
		},
	});

// generate hash
userSchema.methods.generateHash = function(password) {
	return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// check for valid password
userSchema.methods.validPassword = function(password) {
	return bcrypt.compareSync(password, this.local.password);
};

// create model and expose
module.exports = mongoose.model('User', userSchema);

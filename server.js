// set up
var express = require('express'),
	app = express(),
	session = require('express-session'),
	bodyParser = require('body-parser'),
	path = require('path'),
	port = process.env.PORT || 80,
	logmode = process.env.LOG_MODE || 'dev',
	cookieSecret = process.env.COOKIE_SECRET || 'chaos theory are the greatest band in the history of mankind',
	sessionSecret = process.env.SESSION_SECRET || 'chaos theory are the best group in the history of humanity',
	mongoose = require('mongoose'),
	passport = require('passport'),
	flash = require('connect-flash'),
	i18n = require('i18next'),
	livereload = require('express-livereload'),
	lrconnect = require('connect-livereload'),
	configDB = require('./config/database.js');

mongoose.connect(configDB.url); // connect db

// set up app
app.use(require('serve-static')(path.join(__dirname, 'public'))); // set public dir for static docs
app.use(require('morgan')(logmode)); // logger
app.use(require('cookie-parser')(cookieSecret)); // read cookies for auth

// get info from html forms
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json({extended: true}));

// configure sessions
app.use(session({
	secret: sessionSecret,
    saveUninitialized: true,
    resave: true,
	key: 'sid',
	cookie: {
		secure: false,
		maxAge: 24*60*60*1000
	}
}));

app.use(i18n.handle); // multi language engine
app.set('view engine', 'ejs'); // set ejs for templating

require('./config/passport')(passport); // pass passport config
app.use(passport.initialize()); // init passport
app.use(passport.session()); // persist logins

if(app.settings.env === 'development') { // setup livereload if in dev environment
	app.use(lrconnect());
	livereload(app);
}

// error handler
app.use(function(err, req, res, next) {
	res.status(500);
	res.render('error', { error: err });
});

// initialize multi language
i18n.init({
	ns: { namespaces: ['ns.common', 'ns.home', 'ns.header', 'ns.auth'], defaultNs: 'ns.common'},
	languages: ['en'],
	supportedLngs: ['en'],
	resGetPath: 'locales/__lng__/__ns__.json',
    resSetPath: 'locales/__lng__/__ns__.json',
	fallbackLng: "en",
	saveMissing: false,
    sendMissingTo: 'fallback',
	debug: true
});

i18n.registerAppHelper(app);

// set routes
require('./app/routes.js')(app, passport); // load routes with app and passport
// launch
app.listen(port);
console.log('Listening on port ' + port);
